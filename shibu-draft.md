# Step Step procedure for implementation rating us from unique ip in laravel app

1. migration  to database
2. passing user_id, ipaddress, appurl as props in vue template
5. making ui
6. making json api









# how to change language using ip address

~~~js

function changeLanguage (lang) {
    if (lang == 'en') {
        $("[data-langen]").each(function (index , val) {
            $(this).html($(this).attr('data-langen'));
        });
    } else {
        $("[data-langhe]").each(function (index , val) {
            $(this).html($(this).attr('data-langhe'));
        });
    }
}
if (localStorage.getItem('user_lang')) {
  let lang = localStorage.getItem('user_lang');
  changeLanguage(lang);
} else {
  $.get('http://ip-api.com/json', (response) => {
  	if (response.countryCode == "IL") {
      localStorage.setItem("user_lang", 'il')
      changeLanguage('he');
  	}
  })
}
~~~

# flatten array in php

~~~php
public function flattenUnique(array $array) {
  $return = array();
  array_walk_recursive($array, function($a) use (&$return) { $return[] = intval($a); });
  return array_unique( $return );
}
~~~

# functions in php

~~~php
array_values()  // it will removed key of associated array
json_decode() // decode json to php  // database array to php array
array_intersect() // common element between 2 array
~~~

# nearby restaurant mysql query

~~~php
 public function restaurant_by_coords () {
  $lat = request('lat');
  $lon = request('lon');
  $distance = 20;
  $mysqlQuery = "SELECT
    settings.id as id,
    settings.title as title,
    settings.phone as phone,
    users.name as name,
    user_id,
    shortcode,
    address,
    logo,
    (
        6371 *
        acos(
            cos( radians( $lat ) ) *
            cos( radians( `lat` ) ) *
            cos(
                radians( `lon` ) - radians( $lon )
            ) +
            sin(radians($lat)) *
            sin(radians(`lat`))
        )
    ) `distance`
FROM
    `settings`
JOIN
  `users`
WHERE
  settings.user_id=users.id
HAVING
    `distance` < $distance
ORDER BY
    `distance`
LIMIT
    100";
  $data = DB::select($mysqlQuery);
  return response()->json($data);
 }
~~~




