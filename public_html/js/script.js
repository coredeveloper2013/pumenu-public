$(function () {
	$('.datepicker').datepicker({
		dateFormat : 'yy-mm-dd'
	});
	$('.timepicker').timepicker({
		timeFormat : 'h:mm p' ,
		interval : 60 ,
		dynamic : false ,
		dropdown : true ,
		scrollbar : true
	});
	
	/* select date if its greater or equal to the date */
	function dateSplit(arrivalDate) {
		if (arrivalDate == 'undefined') {
			alert('invalid date supplied');
			return false;
		} else {
			var chunks = arrivalDate.split('-');
			var dateprams = [];
			dateprams['year'] = chunks[0];
			dateprams['month'] = chunks[1];
			dateprams['day'] = chunks[2];
			return dateprams;
		}
	}
	
	/*calculating differences*/
	function datedifference(fromdate , todate) {
		fromdate = dateSplit(fromdate);
		todate = dateSplit(todate);
		var fDate = moment([fromdate['year'] , fromdate['month'] , fromdate['day']]);
		var toDate = moment([todate['year'] , todate['month'] , todate['day']]);
		var difference = toDate.diff(fDate , 'days');
		return difference;
	}
	
	var arrivalDate = $("input[name='v_in_arrivalDate']").val();
	var depatureDate = $("input[name='v_out_date']").val();
	/* dates calculations */
	$("input[name='v_in_arrivalDate'] , input[name='v_out_date']").on('change' , function () {
		arrivalDate = $("input[name='v_in_arrivalDate']").val();
		depatureDate = $("input[name='v_out_date']").val();
		$("table.table").find('input').val('');
		if (arrivalDate != '' && depatureDate != '') {
			var arrivalDate1 = arrivalDate.replace(/\D/g , '');
			var departureDate1 = depatureDate.replace(/\D/g , '');
			if (departureDate1 <= arrivalDate1) {
				$("input[name='v_out_date']").val('');
				alert('sorry arrival date must be less then departure date');
			}
		}
	});
	/* first date */
	$("input[name='v_dayOne_checkIn']").on('change' , function () {
		var firstDayInput = $(this).val();
		var firstDayArrival = arrivalDate;
		firstDayInput = firstDayInput.replace(/\D/g , '');
		firstDayArrival = firstDayArrival.replace(/\D/g , '');
		if (firstDayInput < (firstDayArrival)) {
			$(this).val('');
			alert('sorry date must be starting from ' + arrivalDate);
		}
	});
	/* last date */
	$("input[name='v_dayThree_checkOut']").on('change' , function () {
		var lastDayInput = $(this).val();
		var lastDayDeparture = depatureDate;
		lastDayInput = lastDayInput.replace(/\D/g , '');
		lastDayDeparture = lastDayDeparture.replace(/\D/g , '');
		console.log(lastDayDeparture);
		console.log(lastDayInput);
		if (lastDayDeparture < lastDayInput) {
			$(this).val('');
			alert('sorry date must be Ending ' + depatureDate);
		}
	});
	/* comparing two values in the table row */
	$('table.table').on('change' , '.datepicker' , function () {
		/* picking both values in the table */
		var firstDate = $(this).closest('tr').find('td:nth-child(5) .datepicker').val();
		var secondDate = $(this).closest('tr').find('td:nth-child(6) .datepicker').val();
		if (firstDate == '' || secondDate == '') $(this).closest('tr > td:last-child input').val('');
		if ((firstDate != '' && secondDate != '')) {
			var diff = datedifference(firstDate , secondDate);
			if (diff < 1) {
				alert('sorry you enter wrong date , please select date greater then : ' + firstDate);
				$(this).closest('tr').find('td:nth-child(6) .datepicker').val('');
				return false;
			}
			$(this).closest('tr').find('td').last().find('input').val(diff);
			$(this).closest('tr').next().find('td:nth-child(5)').find('input').val(secondDate);
		}
	});
	/* change values on click of makkah and madina */
	$("#madina , #makkah").on('click' , function () {
		if ($(this).attr('id') == 'makkah') {
			var Val = 1;
			var Name = 'Makkah';
			var Name2 = 'Madina';
		} else {
			var Val = 2;
			var Name = 'Madina';
			var Name2 = 'Makkah';
		}
		var tbody = $("table.table").find('tbody');
		$(tbody).find("tr:first-child input[name='v_dayOne_city']").val(Name);
		$(tbody).find("tr:nth-child(2) input[name='v_dayTwo_city']").val(Name2);
		$(tbody).find("tr:nth-child(3) input[name='v_dayThree_city']").val(Name);
	});
});


/**/